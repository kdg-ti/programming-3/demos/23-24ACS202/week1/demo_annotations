package be.kdg.programming3;

import be.kdg.programming3.annotations.Heavy;

import java.util.Random;

public class MyHeavyMethodsClass {
    @Heavy(maxTime = 100)
    public static void slowMethod(){
        System.out.println("slow method starting");
        try {
            Thread.sleep(new Random().nextInt(500));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void normalMethod(){
        System.out.println("This is a normal method");
    }

}
